#!/usr/bin/env iocsh.bash

require(s7plc)
require(modbus)
require(calc)
require(common)

epicsEnvSet("TOP", "../cwl_cws01_ctrl_plc_001")
cd $(TOP)

iocshLoad("cwl_cws01_ctrl_plc_001.iocsh", "IPADDR=172.30.5.218, RECVTIMEOUT=2000, cwl_cws01_ctrl_plc_001_VERSION=plcfactory")

# Loading COMMON modules
iocshLoad("$(common_DIR)e3-common.iocsh")
